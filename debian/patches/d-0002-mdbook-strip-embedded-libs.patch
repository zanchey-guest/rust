From: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Date: Sat, 2 Oct 2021 01:08:00 +0100
Subject: d-0002-mdbook-strip-embedded-libs

Comment: Use https://github.com/infinity0/mdBook/tree/debian to help you rebase the patch on top of a newer version. . Make sure the paths here match the ones in debian/rust-doc.links
---
 src/tools/linkchecker/main.rs                      |  28 +++++-
 vendor/mdbook/src/book/init.rs                     |  19 ----
 .../src/renderer/html_handlebars/hbs_renderer.rs   | 110 ++++-----------------
 .../mdbook/src/renderer/html_handlebars/search.rs  |   2 -
 vendor/mdbook/src/theme/index.hbs                  |  99 +------------------
 vendor/mdbook/src/theme/mod.rs                     |  27 -----
 vendor/mdbook/src/theme/searcher/mod.rs            |   2 -
 7 files changed, 47 insertions(+), 240 deletions(-)

diff --git a/src/tools/linkchecker/main.rs b/src/tools/linkchecker/main.rs
index 4170c32..d7dcda7 100644
--- a/src/tools/linkchecker/main.rs
+++ b/src/tools/linkchecker/main.rs
@@ -159,7 +159,17 @@ impl Checker {
         for entry in t!(dir.read_dir()).map(|e| t!(e)) {
             let path = entry.path();
             // Goes through symlinks
-            let metadata = t!(fs::metadata(&path));
+            let metadata = fs::metadata(&path);
+            if let Err(err) = metadata {
+                if let Ok(target) = fs::read_link(&path) {
+                    if target.starts_with("/usr/share") {
+                        // broken symlink to /usr/share, ok for our Debian build
+                        return;
+                    }
+                }
+                panic!("error at file {:?} while walking - {:?}", path, err)
+            }
+            let metadata = t!(metadata);
             if metadata.is_dir() {
                 self.walk(&path, report);
             } else {
@@ -172,7 +182,15 @@ impl Checker {
     fn check(&mut self, file: &Path, report: &mut Report) {
         let (pretty_path, entry) = self.load_file(file, report);
         let source = match entry {
-            FileEntry::Missing => panic!("missing file {:?} while walking", file),
+            FileEntry::Missing => {
+                if let Ok(target) = fs::read_link(&file) {
+                    if target.starts_with("/usr/share") {
+                        // broken symlink to /usr/share, ok for our Debian build
+                        return;
+                    }
+                }
+                panic!("missing file {:?} while walking", file)
+            }
             FileEntry::Dir => unreachable!("never with `check` path"),
             FileEntry::OtherFile => return,
             FileEntry::Redirect { .. } => return,
@@ -238,6 +256,12 @@ impl Checker {
             let (target_pretty_path, target_entry) = self.load_file(&path, report);
             let (target_source, target_ids) = match target_entry {
                 FileEntry::Missing => {
+                    if let Ok(target) = fs::read_link(&path) {
+                        if target.starts_with("/usr/share") {
+                            // broken symlink to /usr/share, ok for our Debian build
+                            return;
+                        }
+                    }
                     if is_exception(file, &target_pretty_path) {
                         report.links_ignored_exception += 1;
                     } else {
diff --git a/vendor/mdbook/src/book/init.rs b/vendor/mdbook/src/book/init.rs
index ebcdd93..41dab42 100644
--- a/vendor/mdbook/src/book/init.rs
+++ b/vendor/mdbook/src/book/init.rs
@@ -153,25 +153,6 @@ impl BookBuilder {
         let mut js = File::create(themedir.join("book.js"))?;
         js.write_all(theme::JS)?;
 
-        let mut highlight_css = File::create(themedir.join("highlight.css"))?;
-        highlight_css.write_all(theme::HIGHLIGHT_CSS)?;
-
-        let mut highlight_js = File::create(themedir.join("highlight.js"))?;
-        highlight_js.write_all(theme::HIGHLIGHT_JS)?;
-
-        write_file(&themedir.join("fonts"), "fonts.css", theme::fonts::CSS)?;
-        for (file_name, contents) in theme::fonts::LICENSES {
-            write_file(&themedir, file_name, contents)?;
-        }
-        for (file_name, contents) in theme::fonts::OPEN_SANS.iter() {
-            write_file(&themedir, file_name, contents)?;
-        }
-        write_file(
-            &themedir,
-            theme::fonts::SOURCE_CODE_PRO.0,
-            theme::fonts::SOURCE_CODE_PRO.1,
-        )?;
-
         Ok(())
     }
 
diff --git a/vendor/mdbook/src/renderer/html_handlebars/hbs_renderer.rs b/vendor/mdbook/src/renderer/html_handlebars/hbs_renderer.rs
index e170e2f..caa2eff 100644
--- a/vendor/mdbook/src/renderer/html_handlebars/hbs_renderer.rs
+++ b/vendor/mdbook/src/renderer/html_handlebars/hbs_renderer.rs
@@ -3,13 +3,14 @@ use crate::config::{BookConfig, Config, HtmlConfig, Playground, RustEdition};
 use crate::errors::*;
 use crate::renderer::html_handlebars::helpers;
 use crate::renderer::{RenderContext, Renderer};
-use crate::theme::{self, playground_editor, Theme};
+use crate::theme::{self, Theme};
 use crate::utils;
 
 use std::borrow::Cow;
 use std::collections::BTreeMap;
 use std::collections::HashMap;
 use std::fs::{self, File};
+use std::os::unix::fs::symlink;
 use std::path::{Path, PathBuf};
 
 use crate::utils::fs::get_404_output_file;
@@ -235,105 +236,28 @@ impl HtmlHandlebars {
         if let Some(contents) = &theme.favicon_svg {
             write_file(destination, "favicon.svg", contents)?;
         }
-        write_file(destination, "highlight.css", &theme.highlight_css)?;
         write_file(destination, "tomorrow-night.css", &theme.tomorrow_night_css)?;
         write_file(destination, "ayu-highlight.css", &theme.ayu_highlight_css)?;
-        write_file(destination, "highlight.js", &theme.highlight_js)?;
-        write_file(destination, "clipboard.min.js", &theme.clipboard_js)?;
-        write_file(
-            destination,
-            "FontAwesome/css/font-awesome.css",
-            theme::FONT_AWESOME,
-        )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/fontawesome-webfont.eot",
-            theme::FONT_AWESOME_EOT,
-        )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/fontawesome-webfont.svg",
-            theme::FONT_AWESOME_SVG,
+        symlink(
+            "/usr/share/fonts-font-awesome/css/font-awesome.min.css",
+            destination.join("css/font-awesome.min.css"),
         )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/fontawesome-webfont.ttf",
-            theme::FONT_AWESOME_TTF,
+        symlink(
+            "/usr/share/fonts-font-awesome/fonts",
+            destination.join("fonts"),
         )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/fontawesome-webfont.woff",
-            theme::FONT_AWESOME_WOFF,
+        symlink(
+            "/usr/share/javascript/highlight.js/styles/atelier-dune-light.css",
+            destination.join("highlight.css"),
         )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/fontawesome-webfont.woff2",
-            theme::FONT_AWESOME_WOFF2,
+        symlink(
+            "/usr/share/javascript/highlight.js/highlight.js",
+            destination.join("highlight.js"),
         )?;
-        write_file(
-            destination,
-            "FontAwesome/fonts/FontAwesome.ttf",
-            theme::FONT_AWESOME_TTF,
+        symlink(
+            "/usr/share/javascript/mathjax/MathJax.js",
+            destination.join("MathJax.js"),
         )?;
-        if html_config.copy_fonts {
-            write_file(destination, "fonts/fonts.css", theme::fonts::CSS)?;
-            for (file_name, contents) in theme::fonts::LICENSES.iter() {
-                write_file(destination, file_name, contents)?;
-            }
-            for (file_name, contents) in theme::fonts::OPEN_SANS.iter() {
-                write_file(destination, file_name, contents)?;
-            }
-            write_file(
-                destination,
-                theme::fonts::SOURCE_CODE_PRO.0,
-                theme::fonts::SOURCE_CODE_PRO.1,
-            )?;
-        }
-        if let Some(fonts_css) = &theme.fonts_css {
-            if !fonts_css.is_empty() {
-                if html_config.copy_fonts {
-                    warn!(
-                        "output.html.copy_fonts is deprecated.\n\
-                        Set copy_fonts=false and ensure the fonts you want are in \
-                        the `theme/fonts/` directory."
-                    );
-                }
-                write_file(destination, "fonts/fonts.css", &fonts_css)?;
-            }
-        }
-        if !html_config.copy_fonts && theme.fonts_css.is_none() {
-            warn!(
-                "output.html.copy_fonts is deprecated.\n\
-                This book appears to have copy_fonts=false without a fonts.css file.\n\
-                Add an empty `theme/fonts/fonts.css` file to squelch this warning."
-            );
-        }
-        for font_file in &theme.font_files {
-            let contents = fs::read(font_file)?;
-            let filename = font_file.file_name().unwrap();
-            let filename = Path::new("fonts").join(filename);
-            write_file(destination, filename, &contents)?;
-        }
-
-        let playground_config = &html_config.playground;
-
-        // Ace is a very large dependency, so only load it when requested
-        if playground_config.editable && playground_config.copy_js {
-            // Load the editor
-            write_file(destination, "editor.js", playground_editor::JS)?;
-            write_file(destination, "ace.js", playground_editor::ACE_JS)?;
-            write_file(destination, "mode-rust.js", playground_editor::MODE_RUST_JS)?;
-            write_file(
-                destination,
-                "theme-dawn.js",
-                playground_editor::THEME_DAWN_JS,
-            )?;
-            write_file(
-                destination,
-                "theme-tomorrow_night.js",
-                playground_editor::THEME_TOMORROW_NIGHT_JS,
-            )?;
-        }
 
         Ok(())
     }
diff --git a/vendor/mdbook/src/renderer/html_handlebars/search.rs b/vendor/mdbook/src/renderer/html_handlebars/search.rs
index a9e2f5c..3e3f69c 100644
--- a/vendor/mdbook/src/renderer/html_handlebars/search.rs
+++ b/vendor/mdbook/src/renderer/html_handlebars/search.rs
@@ -53,8 +53,6 @@ pub fn create_files(search_config: &Search, destination: &Path, book: &Book) ->
             format!("Object.assign(window.search, {});", index).as_bytes(),
         )?;
         utils::fs::write_file(destination, "searcher.js", searcher::JS)?;
-        utils::fs::write_file(destination, "mark.min.js", searcher::MARK_JS)?;
-        utils::fs::write_file(destination, "elasticlunr.min.js", searcher::ELASTICLUNR_JS)?;
         debug!("Copying search files ✓");
     }
 
diff --git a/vendor/mdbook/src/theme/index.hbs b/vendor/mdbook/src/theme/index.hbs
index 6f3948c..7e5c54c 100644
--- a/vendor/mdbook/src/theme/index.hbs
+++ b/vendor/mdbook/src/theme/index.hbs
@@ -33,10 +33,7 @@
         {{/if}}
 
         <!-- Fonts -->
-        <link rel="stylesheet" href="{{ path_to_root }}FontAwesome/css/font-awesome.css">
-        {{#if copy_fonts}}
-        <link rel="stylesheet" href="{{ path_to_root }}fonts/fonts.css">
-        {{/if}}
+        <link rel="stylesheet" href="{{ path_to_root }}css/font-awesome.min.css">
 
         <!-- Highlight.js Stylesheets -->
         <link rel="stylesheet" href="{{ path_to_root }}highlight.css">
@@ -50,7 +47,7 @@
 
         {{#if mathjax_support}}
         <!-- MathJax -->
-        <script async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
+        <script async src="MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
         {{/if}}
     </head>
     <body>
@@ -61,48 +58,6 @@
             var default_theme = window.matchMedia("(prefers-color-scheme: dark)").matches ? "{{ preferred_dark_theme }}" : "{{ default_theme }}";
         </script>
 
-        <!-- Work around some values being stored in localStorage wrapped in quotes -->
-        <script>
-            try {
-                var theme = localStorage.getItem('mdbook-theme');
-                var sidebar = localStorage.getItem('mdbook-sidebar');
-
-                if (theme.startsWith('"') && theme.endsWith('"')) {
-                    localStorage.setItem('mdbook-theme', theme.slice(1, theme.length - 1));
-                }
-
-                if (sidebar.startsWith('"') && sidebar.endsWith('"')) {
-                    localStorage.setItem('mdbook-sidebar', sidebar.slice(1, sidebar.length - 1));
-                }
-            } catch (e) { }
-        </script>
-
-        <!-- Set the theme before any content is loaded, prevents flash -->
-        <script>
-            var theme;
-            try { theme = localStorage.getItem('mdbook-theme'); } catch(e) { }
-            if (theme === null || theme === undefined) { theme = default_theme; }
-            var html = document.querySelector('html');
-            html.classList.remove('no-js')
-            html.classList.remove('{{ default_theme }}')
-            html.classList.add(theme);
-            html.classList.add('js');
-        </script>
-
-        <!-- Hide / unhide sidebar before it is displayed -->
-        <script>
-            var html = document.querySelector('html');
-            var sidebar = null;
-            if (document.body.clientWidth >= 1080) {
-                try { sidebar = localStorage.getItem('mdbook-sidebar'); } catch(e) { }
-                sidebar = sidebar || 'visible';
-            } else {
-                sidebar = 'hidden';
-            }
-            html.classList.remove('sidebar-visible');
-            html.classList.add("sidebar-" + sidebar);
-        </script>
-
         <nav id="sidebar" class="sidebar" aria-label="Table of contents">
             <div class="sidebar-scrollbox">
                 {{#toc}}{{/toc}}
@@ -240,54 +195,8 @@
         </script>
         {{/if}}
 
-        {{#if google_analytics}}
-        <!-- Google Analytics Tag -->
-        <script>
-            var localAddrs = ["localhost", "127.0.0.1", ""];
-
-            // make sure we don't activate google analytics if the developer is
-            // inspecting the book locally...
-            if (localAddrs.indexOf(document.location.hostname) === -1) {
-                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
-                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
-                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
-                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
-
-                ga('create', '{{google_analytics}}', 'auto');
-                ga('send', 'pageview');
-            }
-        </script>
-        {{/if}}
-
-        {{#if playground_line_numbers}}
-        <script>
-            window.playground_line_numbers = true;
-        </script>
-        {{/if}}
-
-        {{#if playground_copyable}}
-        <script>
-            window.playground_copyable = true;
-        </script>
-        {{/if}}
-
-        {{#if playground_js}}
-        <script src="{{ path_to_root }}ace.js"></script>
-        <script src="{{ path_to_root }}editor.js"></script>
-        <script src="{{ path_to_root }}mode-rust.js"></script>
-        <script src="{{ path_to_root }}theme-dawn.js"></script>
-        <script src="{{ path_to_root }}theme-tomorrow_night.js"></script>
-        {{/if}}
-
-        {{#if search_js}}
-        <script src="{{ path_to_root }}elasticlunr.min.js"></script>
-        <script src="{{ path_to_root }}mark.min.js"></script>
-        <script src="{{ path_to_root }}searcher.js"></script>
-        {{/if}}
-
-        <script src="{{ path_to_root }}clipboard.min.js"></script>
-        <script src="{{ path_to_root }}highlight.js"></script>
-        <script src="{{ path_to_root }}book.js"></script>
+        <script src="{{ path_to_root }}highlight.js" type="text/javascript" charset="utf-8"></script>
+        <script src="{{ path_to_root }}book.js" type="text/javascript" charset="utf-8"></script>
 
         <!-- Custom JS scripts -->
         {{#each additional_js}}
diff --git a/vendor/mdbook/src/theme/mod.rs b/vendor/mdbook/src/theme/mod.rs
index 6e6b509..ef8886b 100644
--- a/vendor/mdbook/src/theme/mod.rs
+++ b/vendor/mdbook/src/theme/mod.rs
@@ -1,9 +1,5 @@
 #![allow(missing_docs)]
 
-pub mod playground_editor;
-
-pub mod fonts;
-
 #[cfg(feature = "search")]
 pub mod searcher;
 
@@ -24,19 +20,8 @@ pub static VARIABLES_CSS: &[u8] = include_bytes!("css/variables.css");
 pub static FAVICON_PNG: &[u8] = include_bytes!("favicon.png");
 pub static FAVICON_SVG: &[u8] = include_bytes!("favicon.svg");
 pub static JS: &[u8] = include_bytes!("book.js");
-pub static HIGHLIGHT_JS: &[u8] = include_bytes!("highlight.js");
 pub static TOMORROW_NIGHT_CSS: &[u8] = include_bytes!("tomorrow-night.css");
-pub static HIGHLIGHT_CSS: &[u8] = include_bytes!("highlight.css");
 pub static AYU_HIGHLIGHT_CSS: &[u8] = include_bytes!("ayu-highlight.css");
-pub static CLIPBOARD_JS: &[u8] = include_bytes!("clipboard.min.js");
-pub static FONT_AWESOME: &[u8] = include_bytes!("FontAwesome/css/font-awesome.min.css");
-pub static FONT_AWESOME_EOT: &[u8] = include_bytes!("FontAwesome/fonts/fontawesome-webfont.eot");
-pub static FONT_AWESOME_SVG: &[u8] = include_bytes!("FontAwesome/fonts/fontawesome-webfont.svg");
-pub static FONT_AWESOME_TTF: &[u8] = include_bytes!("FontAwesome/fonts/fontawesome-webfont.ttf");
-pub static FONT_AWESOME_WOFF: &[u8] = include_bytes!("FontAwesome/fonts/fontawesome-webfont.woff");
-pub static FONT_AWESOME_WOFF2: &[u8] =
-    include_bytes!("FontAwesome/fonts/fontawesome-webfont.woff2");
-pub static FONT_AWESOME_OTF: &[u8] = include_bytes!("FontAwesome/fonts/FontAwesome.otf");
 
 /// The `Theme` struct should be used instead of the static variables because
 /// the `new()` method will look if the user has a theme directory in their
@@ -59,11 +44,8 @@ pub struct Theme {
     pub favicon_png: Option<Vec<u8>>,
     pub favicon_svg: Option<Vec<u8>>,
     pub js: Vec<u8>,
-    pub highlight_css: Vec<u8>,
     pub tomorrow_night_css: Vec<u8>,
     pub ayu_highlight_css: Vec<u8>,
-    pub highlight_js: Vec<u8>,
-    pub clipboard_js: Vec<u8>,
 }
 
 impl Theme {
@@ -93,9 +75,6 @@ impl Theme {
                     theme_dir.join("css/variables.css"),
                     &mut theme.variables_css,
                 ),
-                (theme_dir.join("highlight.js"), &mut theme.highlight_js),
-                (theme_dir.join("clipboard.min.js"), &mut theme.clipboard_js),
-                (theme_dir.join("highlight.css"), &mut theme.highlight_css),
                 (
                     theme_dir.join("tomorrow-night.css"),
                     &mut theme.tomorrow_night_css,
@@ -183,11 +162,8 @@ impl Default for Theme {
             favicon_png: Some(FAVICON_PNG.to_owned()),
             favicon_svg: Some(FAVICON_SVG.to_owned()),
             js: JS.to_owned(),
-            highlight_css: HIGHLIGHT_CSS.to_owned(),
             tomorrow_night_css: TOMORROW_NIGHT_CSS.to_owned(),
             ayu_highlight_css: AYU_HIGHLIGHT_CSS.to_owned(),
-            highlight_js: HIGHLIGHT_JS.to_owned(),
-            clipboard_js: CLIPBOARD_JS.to_owned(),
         }
     }
 }
@@ -273,11 +249,8 @@ mod tests {
             favicon_png: Some(Vec::new()),
             favicon_svg: Some(Vec::new()),
             js: Vec::new(),
-            highlight_css: Vec::new(),
             tomorrow_night_css: Vec::new(),
             ayu_highlight_css: Vec::new(),
-            highlight_js: Vec::new(),
-            clipboard_js: Vec::new(),
         };
 
         assert_eq!(got, empty);
diff --git a/vendor/mdbook/src/theme/searcher/mod.rs b/vendor/mdbook/src/theme/searcher/mod.rs
index d5029db..59eda8a 100644
--- a/vendor/mdbook/src/theme/searcher/mod.rs
+++ b/vendor/mdbook/src/theme/searcher/mod.rs
@@ -2,5 +2,3 @@
 //! the "search" cargo feature is disabled.
 
 pub static JS: &[u8] = include_bytes!("searcher.js");
-pub static MARK_JS: &[u8] = include_bytes!("mark.min.js");
-pub static ELASTICLUNR_JS: &[u8] = include_bytes!("elasticlunr.min.js");
